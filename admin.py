# -*- coding: utf-8 -*-

import settings
import json
import base64
import hashlib
import os
import eyed3
import utils.liquidsoap
import utils.statistics as statistics

from flask import Flask, jsonify, request, session, send_from_directory, make_response
from flask.ext import restful
from werkzeug.contrib.cache import MemcachedCache
from asq.initiators import query
from logger import logger
from mongokit import ObjectId
from bson import json_util
from errors import *
from utils.permissions import *
from eyed3.id3 import Tag

from model import db

app = Flask(__name__)
api = restful.Api(app)

# Настраиваем memcached:
cache = MemcachedCache(['{0}:{1}'.format(settings.MEMCACHED_SERVER, settings.MEMCACHED_PORT)])

app.secret_key = settings.SECRET_KEY

liquidsoap = utils.liquidsoap.LiqudsoapAPI()


@app.route('/')
def api_help():
    return '<h1>Deepwave API<h1>'


#
# API Functions
#

@app.route('/api/security/desktop_auth/viwer/<viewer_id>/access_token/<access_token>/version/<version>')
def desktop_admin_auth(viewer_id, access_token, version):
    """
      Ограниченный режим авторизации. Только из приложения администратора.
      В релизе требуется избавиться от данного метода, так как он компрометирует безопасность.

      Коды ошибок:

        1 - Auth failed
        2 - User not found
        500 - Internal Server Error
    """

    try:
        md5 = hashlib.md5()

        md5.update('{0}_{1}'.format(viewer_id, settings.SECRET_SIGN))

        if md5.hexdigest() != access_token:
            response = {
                'code': 1,
                'error': 'Auth failed'
            }

            return json.dumps(response)

        user = db.UserEntity.find_one({'user_id': viewer_id})

        if user is None:
            response = {
                'code': 2,
                'error': 'User not found.'
            }

            return json.dumps(response)

        try:
            version_db = query(list(db.VersionEntity.find())).last_or_default(None)

            current_version_db = '{0}.{1}'.format(version_db.major, version_db.minor)
            logger.info('Version from db: {0}\nVersion from program: {1}'.format(current_version_db, version))

            if version_db is None or current_version_db != version:
                is_version_valid = False
            else:
                is_version_valid = True
        except Exception as ex:
            logger.error(ex.message)
            is_version_valid = False

        session['user'] = {
            '_id': str(user.get_safe("_id")),
            'user_id': user.get_safe("user_id"),
            'first_name': user.get_safe("first_name"),
            'last_name': user.get_safe('last_name'),
            'owns': user.get_safe('owns', []),
            'is_auth': True,
            'is_super_root': user.get_safe('is_super_root', False),
            'is_version_valid': is_version_valid
        }

        logger.info("user '{0}' login success".format(viewer_id))
        logger.info(session['user'])

        return json.dumps({
            'status': 'success'
        })

    except Exception as ex:
        logger.exception(ex)

        return json.dumps({
            'code': 500,
            'error': 'Internal Server Error'
        })


@app.route('/api/track/download/<track_name>')
def get_track(track_name):
    """
    Отдает файл трэка
    """
    return send_from_directory(app.config['UPLOAD_FOLDER'], '{0}.mp3'.format(track_name))


@app.route('/api/radio/get_current_song/<station_id>')
def get_current_song(station_id):
    """
    Возвращает название играющего сейчас трэка на канале
    """

    node = cache.get('{0}_tracks_cache'.format(station_id))

    if node is None or len(node) == 0:
        return ''

    split = node[-1].split('|')
    track = {
        "track_id": split[-1],
        "track_name": split[0]
    }

    return json.dumps(track)


#
# Утилиты для обслуживания станции
#

@app.route('/api/radio/check_station/<station_id>')
def check_station(station_id):
    """
    Проверяет все ли трэки заявленные в плейлисте существуют на сервере

    Ошибки:
        404 - станция не найдена
        550 - internal server error
    """
    station_dir = settings.MUSIC_DIR_PATH.format(station_id)

    # Получаем все трэки станции
    station = db.RadioStationEntity.find_one({'station_id': station_id})

    if not check_owned(session, station['station_id']):
        return get_operation_permission_denided()

    if station is None:
        return json.dumps({
            'code': 404,
            'error': "Station {0} not found".format(station_id)
        })

    not_exists_count = 0
    for track in station.get_safe('tracks'):
        if not os.path.exists('{0}{1}.mp3'.format(station_dir, track['hash'])):
            track['hash'] = ''
            not_exists_count += 1

    try:
        station.save()
    except Exception as ex:
        return json.dumps({
            'code': 550,
            'error': 'Internal Sever Error'
        })

    return json.dumps({
        'status': 'ok',
        'not_exists_count': not_exists_count
    })


#
# Управление IceCast 2 и LiquidSoap
#

@app.route('/api/playlist/get/station/<station_id>')
def get_playlist(station_id):
    """
    Отправляем на сервер плейлист
    """
    if not check_owned(session, station_id):
        return get_operation_permission_denided()

    station = db.RadioStationEntity.find_one({'station_id': station_id})

    if station is None:
        return False

    with open("{0}{1}.pls".format(settings.PLAYLIST_DIR_PATH, station_id), "r") as f:
        lines = f.readlines()

        playlist = []

        for line in lines:
            track_name = os.path.basename(line).split('.')

            # Неверный формат файла. Должен быть <name>.mp3
            if len(track_name) < 2:
                continue

            track = query(station['tracks']).where(lambda x: x['hash'] == track_name[0]).first_or_default(None)

            if track is None:
                continue

            playlist.append({
                'track_id': track['hash'],
                'title': track['title'],
                'artist': track['artist'],
                'duration': track['duration'],
                'statistics': statistics.get_full_statistic_by_track(station_id, track['hash'])
            })

        return json.dumps(playlist)


@app.route('/api/liquidsoap/stop/<station_id>')
def stop_station(station_id):
    if not check_owned(session, station_id):
        return get_operation_permission_denided()

    return liquidsoap.stop(station_id)


@app.route('/api/liquidsoap/start/<station_id>')
def start_station(station_id):
    if not check_owned(session, station_id):
        return get_operation_permission_denided()

    return liquidsoap.start(station_id)


@app.route('/api/liquidsoap/reload/<station_id>')
def reload_station(station_id):
    if not check_owned(session, station_id):
        return get_operation_permission_denided()

    return liquidsoap.reload_station(station_id)


@app.route('/api/liquidsoap/skip/<station_id>')
def skip_track(station_id):
    if not check_owned(session, station_id):
        return get_operation_permission_denided()

    return liquidsoap.skip(station_id)


def update_playlist(station_id):
    """
       Обновляем плейлист станции
    """

    station = db.RadioStationEntity.find_one({'station_id': station_id})

    if station is None:
        return False

    with open("{0}{1}.pls".format(settings.PLAYLIST_DIR_PATH, station_id), "w") as f:
        for track in station.get_safe("tracks", []):
            folder = settings.MUSIC_DIR_PATH.format(station_id)
            file_path = "{0}{1}.mp3".format(folder, track['hash'])

            f.write("{0}\n".format(file_path))

    return True


#
# REST Functions
#

def output_json(data, code, headers=None):
    """Makes a Flask response with a JSON encoded body"""
    resp = make_response(json.dumps(data, default=json_util.default), code)
    resp.headers.extend(headers or {})

    return resp


class BaseApi(restful.Resource):
    representations = {'application/json': output_json}


class TrackApi(BaseApi):
    """
    Функции по работе с трэком

    Коды ошибок:

        1 - Station not found (404)
        2 - Track not found (404)
        100 - Old database version
        500 - Internal server error
    """

    def get(self, track_id, station_id):
        try:
            station = db.RadioStationEntity.find_one({'station_id': station_id})

            if station is None:
                return {
                    'code': 1,
                    'error': "Station '{0}' not found. (code 404)".format(station_id)
                }

            if track_id == "all" or track_id == "-1":
                tracks = list(station["tracks"])

                return tracks

            track = query(station["tracks"])\
                .where(lambda track: track["_id"] == ObjectId(track_id))\
                .first_or_default(None)

            if track is None:
                return {
                    'code': 2,
                    'error': "Track '{0}' not found. (code 404)".format(track_id)
                }

            return track

        except Exception as ex:
            logger.exception(ex)
            return get_internal_server_error_msg()

    def post(self, track_id, station_id):
        """
        Коды ошибок:
            1 - Incorrect data.
            2 - File already exists
        """

        try:

            if not check_owned(session, station_id):
                return get_operation_permission_denided()

            if not check_version(session):
                return get_version_db_is_old()


            response = json.loads(request.data)

            if response is None or 'file' not in response:
                return {
                    'code': 1,
                    'error': 'Incorrect data.'
                }

            content = base64.decodestring(response['file'])

            folder = settings.MUSIC_DIR_PATH.format(station_id)
            file_name = "{0}{1}.mp3".format(folder, track_id)

            # Создаем каталог, если он еще не существует:
            if not os.path.exists(folder):
                os.makedirs(folder)

            # if os.path.exists(file_name):
            #     return {
            #         'code': 2,
            #         "error": "File already exists."
            #     }

            # Сохраняем в файл каталог канала:
            file_handle = open(file_name, "w+b")
            file_handle.write(content)
            file_handle.close()

            bit_rate = 0
            try:
                # Запишем ID3 тэги:
                audiofile = eyed3.load(file_name)

                audiofile.tag = Tag()

                audiofile.tag.artist = station_id
                audiofile.tag.title = track_id

                audiofile.tag.save(filename=file_name, encoding="utf-8")

                bit_rate = audiofile.info.bit_rate[1]

            except:
                logger.error("Tag write '{0}' exception.".format(file_name))

            return {
                'bitrate': bit_rate,
                'status': 'success'
            }
        except Exception as ex:
            logger.exception(ex)

            return get_internal_server_error_msg()


class StationGroupApi(BaseApi):

    def get(self, group_id):
        """
        Коды ошибок:

        404 - Not found.
        """
        try:
            if group_id == "-1" or group_id == "" or group_id == "all":
                return list(db.RadioStationGroupEntity.find())

            group = db.RadioStationGroupEntity.find_one({'group_id': group_id})

            if group is None:
                return  {
                    'code': 404,
                    'error': 'Group not found.'
                }

            return group

        except Exception as ex:
            logger.log(ex)

            return get_internal_server_error_msg()

    def put(self, group_id):
        try:
            if not check_admin(session):
                return get_operation_permission_denided()

            if not check_version(session):
                return get_version_db_is_old()

            group_entity = db.RadioStationGroupEntity.from_json(request.data)

            group_entity.validate()
            group_entity.save()

            return {
                'status': 'success'
            }

        except Exception as ex:
            logger.exception(ex)

            return get_internal_server_error_msg()


class StationApi(BaseApi):

    def get(self, station_id):
        """
        Коды ошибок:

            404 - Not Found.
            500 - Internal server error
        """

        try:
            if station_id == "-1" or station_id == "" or station_id == "all":
                allowed_station = [station for station in db.RadioStationEntity.find() if check_owned(session, station['station_id'])]

                return allowed_station

            station = db.RadioStationEntity.find_one({'station_id': station_id})

            if station is None:
                return {
                    'code': 404,
                    'error': 'Station not found.'
                }
            elif check_owned(session, station['station_id']):
                return get_operation_permission_denided()

            return station
        except Exception as ex:
            logger.exception(ex)

            return get_internal_server_error_msg()

    def put(self, station_id):
        """
        Коды ошибок:

            1 - Incorrect ID
            2 - Station already exists
            3 - User not found
            500 - Internal server error
            550 - Permission denided

        """
        try:
            if not check_version(session):
                return get_version_db_is_old()

            station = db.RadioStationEntity.from_json(request.data)

            if station_id != station['station_id']:
                return {
                    'code': 1,
                    'error': "Incorrect id. '{0}' != '{1}'".format(station_id, station['station_id'])
                }

            station_find = db.RadioStationEntity.find_one({'station_id': station.get_safe('station_id', '')})

            # Если станиция уже есть в БД и id не совпадают с id новой. То генерируем ошибку.
            if station_find is not None:
                if station_find['_id'] != station['_id']:
                    return {
                        'code': 2,
                        'error': "Station with id = '{0}' already exists.".format(station_id)
                    }

                # Проверим имеет ли пользователь права изменять станцию.
                if not check_owned(session, station_find['station_id']):
                    return get_operation_permission_denided()
            else:
                user = session['user']

                # Обновляем пользователя
                user_entity = db.UserEntity.find_one({'user_id': user['user_id']})

                if user_entity is None:
                    return {
                        'error': "User '{0}' not found.".format(user['user_id'])
                    }

                user_entity['owns'] = user_entity.get_safe('owns', []) + [station_id]

                user_entity.validate()
                user_entity.save()

            station.validate()
            station.save()

            # Обновляем файл плейлиста:
            update_playlist(station_id)

            return {
                'status': 'success'
            }

        except Exception as ex:
            logger.exception(ex)

            return get_internal_server_error_msg()


class ModeratorApi(BaseApi):
    def get(self, station_id):

        station = db.RadioStationEntity.find_one({'station_id': station_id})

        if station is None:
            return  {
                'code': 404,
                'error': 'Station not found'
            }

        users = list(db.UserEntity.find({'owns': {'$in': [station_id]}}))

        return users

    def put(self, station_id):
        """
        Коды ошибок:

        1   - Плохой запрос (не удалось получить user_id)
        2   - Пользователь уже является модератором канала
        404 - Пользователь не найден
        500 - Внутренняя ошибка. Возникает при сохранении записи в БД
        550 - Permission denided.
        """

        if not check_owned(session, station_id):
            return get_operation_permission_denided()

        if not check_version(session):
            return get_version_db_is_old()

        user = db.UserEntity.from_json(request.data)

        user_id = user.get_safe('user_id', None)

        if user_id is None:
            return {
                'code': 1,
                'error': "Bad request."
            }

        moderator = db.UserEntity.find_one({'user_id': user_id})

        if moderator is None:
            return {
                'code': 404,
                'error': "User not found."
            }

        if station_id in moderator.get_safe("owns", []):
            return {
                'code': 2,
                'error': "User is already a moderator channel"
            }
        else:
            try:
                moderator["owns"] = moderator.get_safe("owns", []) + [station_id]

                moderator.validate()
                moderator.save()

                return {
                    "status": 'success'
                }
            except Exception as ex:
                logger.exception("Dedication user '{0}' to moderators is failed (station '{1}')".format(user_id,
                                                                                                        station_id), ex)


class GenreApi(BaseApi):
    def get(self, genre_id):

        genres = list(db.GenreEntity.find())

        return genres


# Register all resources:
api.add_resource(TrackApi, '/rest/track/<string:track_id>/station/<string:station_id>')
api.add_resource(StationApi, '/rest/station/<string:station_id>')
api.add_resource(StationGroupApi, '/rest/group/<string:group_id>')
api.add_resource(ModeratorApi, '/rest/moderator/station/<string:station_id>')
api.add_resource(GenreApi, '/rest/genre/<string:genre_id>/')


if __name__ == '__main__':
    app.run(host="127.0.0.1", port=5000, debug=True)

    #print get_playlist('pank')







