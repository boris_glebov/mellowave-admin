from distutils.core import setup

setup(
    name='mellowave-admin',
    version='0.1.0',
    packages=['dto', 'utils'],
    url='https://bitbucket.org/boris_glebov/mellowave-admin',
    license='',
    author='Boris Glebov',
    author_email='avatar29A@gmail.com',
    description='Web service for deepwave project'
)
