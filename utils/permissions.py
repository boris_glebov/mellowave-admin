# -*- coding: utf-8 -*-
__author__ = 'Boris'

from asq.initiators import query

from logger import logger


def check_auth(session):
    return session is None or not 'user' is session or session['is_auth']


def check_admin(session):
    if not check_auth(session):
        return False

    user = session['user']

    return 'is_super_root' in user and user['is_super_root']


def check_owned(session, station_id):
    if not check_auth(session):
        return False

    user = session['user']

    return ('owns' in user and len(user['owns']) > 0 and query(user['owns']).any(lambda x: x == station_id)) \
        or user['is_super_root']


def check_version(session):
    user = session['user']

    return user is not None and 'is_version_valid' in user and user['is_version_valid']