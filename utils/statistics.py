# -*- coding: utf-8 -*-
__author__ = 'Boris'

from model import db


def get_full_statistic_by_track(station_id, track_id):
    try:
        statistics = list(db.StatisticEntity.find({'track_id': track_id}))

        # Получим кол-во прослушиваний. Событие: track_scrobble_event
        votes = [stat for stat in statistics if stat['type'] == 'track_scrobble_event']
        favs = [stat for stat in statistics if stat['type'] == 'favorite_track_event']
        bans = [stat for stat in statistics if stat['type'] == 'block_track_event']

        return {
            'votes': len(votes),
            'favs': len(favs),
            'ban': len(bans)
        }
    except:
        return {
            'votes': 0,
            'favs': 0,
            'ban': 0
        }
