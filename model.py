# -*- coding: utf-8 -*-
__author__ = 'Boris'

from mongokit import Document, Connection, ObjectId
import settings

db = Connection(u"{0}:{1}".format(settings.MONGO_SERVER, settings.MONGO_PORT), replicaset='rs0')


class Entity(Document):
    __database__ = settings.MONGO_DATABASE_NAME

    use_dot_notation = True
    use_schemaless = True

    def get_safe(self, key, default=None):

        last_node = self
        for node in key.split('.'):
            try:
                last_node = last_node.get(node)
            except:
                last_node = default
                break

        return default if last_node == self or last_node is None else last_node

    @property
    def id(self):
        return self._id



@db.register
class TrackEntity(Entity):
    __collection__ = "Tracks"

    structure_info = {
        'title': unicode,
        'artist': unicode,
        'vkid': unicode,
        'vk_url': unicode,
        'duration': int,
        'mbid': unicode,
        'file_name': unicode,
        'hash': unicode
    }


@db.register
class RadioStationEntity(Entity):
    """
    Радио станция
    """
    __collection__ = "RadioStation"

    structure_info = {
        'name': basestring,
        'group': basestring,
        'notes': basestring,
        'url': basestring,
        'playlists': [{
                          'name': basestring,
                          'path': basestring,
                          'tracks': [{
                                         'vkid': basestring,
                                         'title': basestring,
                                         'artist': basestring,
                                         'vk_url': basestring,
                                         'duration': int,
                                         'track_info': TrackEntity
                                     }]
                      }]
    }


@db.register
class VersionEntity(Entity):
    """
    Версия БД
    """

    __collection__ = "Version"
    use_dot_notation = True

    structure = {
        "minor": basestring,
        "major": basestring
    }

@db.register
class RadioStationGroupEntity(Entity):
    __collection__ = "RadioStationGroups"

@db.register
class GenreEntity(Entity):
    __collection__ = "Genres"

@db.register
class UserEntity(Entity):
    __collection__ = "Users"


@db.register
class StatisticEntity(Entity):
    __collection__ = 'Statistics'