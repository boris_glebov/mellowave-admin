# -*- coding: utf-8 -*-
__author__ = 'Boris'

def get_internal_server_error_msg():
    return {
        'code': 500,
        'error': 'Internal server error.'
    }


def get_operation_permission_denided():
    return {
        'code': 550,
        'error': 'Permission denied.'
    }


def get_version_db_is_old():
    return {
        'code': 100,
        'error': "You have an old version of the program. Please update software."
    }
