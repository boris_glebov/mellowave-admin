# -*- coding: utf-8 -*-
__author__ = 'Boris'

import os
from sys import platform as _platform


VERSION = "0.7.3"

#
# ОБЩИЕ НАСТРОЙКИ ПРИЛОЖЕНИЯ
#

# Если приложение запущено в режиме разработки, выставить в True
IS_DEBUG = True

SECRET_KEY = '\xc8y\xed(\xd1\x8f\xfd\xfd\xe1\xfd\xbb={\x07\x0c\xba\x90\xd0\xb2\xb4\xda\xc5\xd0\xfc'
SECRET_SIGN = 'FA5PawusenEgachukEwupeSwasWaKa2ARuThEDethuc7sPAgEQ7cReFrekeB4c4u'

if _platform == "win32":
    LOG_PATH = "c:\\logs\\mellowave-admin.log"
    MUSIC_DIR_PATH = "c:\\radio\\{0}\\"
    PLAYLIST_DIR_PATH = "c:\\radio\\"
else:
    MUSIC_DIR_PATH = "/opt/radio/{0}/"
    LOG_PATH = "/var/log/mellowave-admin.log"
    PLAYLIST_DIR_PATH = "/opt/playlist/"

#
# НАСТРОЙКИ БАЗЫ ДАННЫХ (MongoDB)
#

if IS_DEBUG:
    # Название базы данных
    MONGO_DATABASE_NAME = "radio_db"
    # IP или DNS адрес сервера СУБД
    MONGO_SERVER = "rs0.mellowave.ru"
    # Порт сервера СУБД
    MONGO_PORT = 27017
else:
    # Название базы данных
    MONGO_DATABASE_NAME = "radio_db"
    # IP или DNS адрес сервера СУБД
    MONGO_SERVER = "rs0.mellowave.ru"
    # Порт сервера СУБД
    MONGO_PORT = 27017

#
# НАСТРОЙКИ ICECAST 2 (РАДИО СЕРВЕР)
#

ICECAST_ADMIN_URL = "http://sr1.mellowave.ru:8000/admin/stats?mount=/{0}"
ICECAST_ADMIN_USERNAME = "admin"
ICECAST_ADMIN_PASSWORD = "warava29A"

#
# MEMCHACHED
#

if IS_DEBUG:
    MEMCACHED_SERVER = '192.168.1.100'
else:
    MEMCACHED_SERVER = '127.0.0.1'

MEMCACHED_PORT = 11211

#
# LIQUIDSOAP (Telnet)
#
LIQUIDSOAP_SERVER = 'localhost'
LIQUIDSOAP_PORT = 1234